import cv2 as cv
import numpy as np
import cv2.aruco as aruco
import matplotlib.pyplot as  plt

id_to_find = 72
marker_size = 10

calib_path = "../camera_calibration/"
camera_matrix = np.loadtxt(calib_path + 'cameraMatrix.txt', delimiter=',')
camera_distortion = np.loadtxt(calib_path + 'cameraDistortion.txt', delimiter=',')

R_flip = np.zeros((3, 3), dtype=np.float32)
R_flip[0, 0] = 1.0
R_flip[1, 1] = -1.0
R_flip[2, 2] = -1.0

aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
parameters = aruco.DetectorParameters_create()

cap = cv.VideoCapture(0)
frame = cv.imread('frame_0.jpg', 1)
# while True:
# ret, frame =cap.read()
gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

# try:
corners, ids, rejected = aruco.detectMarkers(image=gray, dictionary=aruco_dict, parameters=parameters,
                                             cameraMatrix=camera_matrix, distCoeff=camera_distortion)

cv.circle(frame, (corners[0][0, 0, :][0], corners[0][0, 0, :][1]), 5, (0, 255, 0), -1)
cv.circle(frame, (corners[0][0, 1, :][0], corners[0][0, 1, :][1]), 5, (0, 255, 0), -1)
cv.circle(frame, (corners[0][0, 2, :][0], corners[0][0, 2, :][1]), 5, (0, 255, 0), -1)
cv.circle(frame, (corners[0][0, 3, :][0], corners[0][0, 3, :][1]), 5, (0, 255, 0), -1)
aruco.drawDetectedMarkers(frame, corners)

ret = aruco.estimatePoseSingleMarkers(corners, marker_size, camera_matrix, camera_distortion)
rvec, tvec = ret[0][0, 0, :], ret[1][0, 0, :]
aruco.drawAxis(frame, camera_matrix, camera_distortion, rvec, tvec, 5)
# Converts a rotation matrix to a rotation vector or vice versa.
R_ct    = cv.Rodrigues(rvec)[0]
R_tc    = R_ct.T

frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
plt.imshow(frame)
plt.show()

# except:
#    pass

# cv.imshow("Frame", frame)
# cv.waitKey()

# cv.destroyAllWindows()
