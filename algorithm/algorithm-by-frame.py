# Author : José Antonio García Acosta.
# Date: september 5th, 2019.
# Title: "Algoritmo para la estimación de coordenadas tridimensionales con visión artificial".

import cv2 as cv
import imutils
import math
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.signal import find_peaks

# Set printer properties
# np.set_printoptions(threshold=sys.maxsize)
# np.set_printoptions(precision=2)
# np.set_printoptions(suppress=True)

def im_show(data):
    for i in range(len(data)):
        plt.figure(i + 4)
        plt.imshow(data[i], cmap="gray")
        # cv.imshow("Frame " + str(i), data[i])
    # cv.waitKey(0)


def contour_analysis(contours, height, width):
    mask = np.zeros((height, width), np.uint8)
    for i in range(len(contours)):
        try:
            [cx, cy] = get_centroid(contours[i])
            cv.circle(mask, (cx, cy), 2, (255, 255, 255), -1)
            cv.drawContours(mask, contours, i, (255, 255, 255), 1)
        except:
            pass
    return mask


def get_signature(contours, ax1):
    normalized_signature = np.loadtxt('normalized_signature.txt',delimiter=',')
    for i in range(len(contours)):
        try:
            centroid = np.array(get_centroid(contours[i])).astype('float64') # Get centroid pixel coordinates
            # Change dtype to float32
            contour_coordiantes = np.asmatrix(contours[i], dtype='float64')  # Get the contour pixel coordinates.
            #contour_coordiantes = np.vstack([contour_coordiantes,contour_coordiantes[0]]) # Close the contour when it's added the 1st element to the last position. As MATLAB does it.
            translated_contour_coordinates = contour_coordiantes - centroid # Move the contour coordinates minus the origin. Why?
            index = np.argsort(translated_contour_coordinates[:,0],axis=0) # Get a sorted array with the index positions.
            translated_contour_coordinates = np.squeeze(translated_contour_coordinates[index]) # Remove one dimension for computation purpose.
            x_contour = np.asarray(translated_contour_coordinates[:,0])
            y_contour = np.asarray(translated_contour_coordinates[:,1])
            signature = cart2pol(x_contour,y_contour)
            # Sort the data by angle, from - PI to PI
            signature_index = np.argsort(signature[:,0],axis=0)
            signature = np.squeeze(signature[signature_index])
            signature = smooth_signature(signature, 5)
            # Normalize the signature
            signature[:,1] = signature[:,1]/np.max(signature[:,1])
            peaks_index = find_corners(signature.copy())
            translated_contour_coordinates = translated_contour_coordinates + centroid
            if len(peaks_index) == 3: #1, 7
                # Correlation filter
                r = np.correlate(normalized_signature[:,1],signature[:,1],'full')
                if np.max(r) >= 20:
                    ax1.clear()
                    plt.plot(signature[:, 0], signature[:, 1])
                    plt.plot(signature[peaks_index][:, 0], signature[peaks_index][:, 1], "or")
                    plt.title('ID Object: ' + str(i))
                    plt.xlabel(r"$\theta$")
                    plt.ylabel(r"$\rho$")
                    plt.pause(0.000001)

                    corners = [translated_contour_coordinates[signature_index][peaks_index, 0],
                       translated_contour_coordinates[signature_index][peaks_index, 1]]
                    corners = np.squeeze(np.asarray(corners)).T
                    corners = np.append([[centroid[0],centroid[1]]], corners, axis = 0)
                    #corners = corners.astype(int)
                    return [i, corners]
        except:
            pass

def pose_estimation(objPoints, imgPoints):
    pass

def smooth_signature(signature, smooth_factor):
    a = convolve(signature[:,1], smooth_factor)
    b = convolve(np.ones(len(signature[:,1]), dtype='float64'),smooth_factor)
    signature[:,1] = a/b
    return signature

def convolve(u,l):
    v = np.ones(l, dtype='float64')
    n_pad = l -1
    full = np.convolve(u,v,'full')
    first = n_pad - n_pad//2
    return full[first:first+len(u)]


def find_corners(signature):
    magnitude = signature[:,1]
    m = np.mean(magnitude)
    #pfind[pfind < m] = 0
    peaks_index, _ = find_peaks(magnitude)
    return peaks_index


def get_centroid(contour):
    M = cv.moments(contour)
    Cx = int(M["m10"] / M["m00"])
    Cy = int(M["m01"] / M["m00"])
    return [Cx, Cy]

def cart2pol(x, y):
    rho = np.sqrt(np.power(x,2) + np.power(y,2))
    phi = np.arctan2(y, x)
    return np.column_stack((np.squeeze(phi),np.squeeze(rho)))

def filter_contours_mean_size(contours):
    filtered_contours = []
    contours_size = 0
    for i in range(len(contours)):
        contours_size += contours[i].shape[0]

    mean_contours_size = round (contours_size / len(contours))

    for i in range(len(contours)):
        if contours[i].shape[0] > mean_contours_size:
            filtered_contours.append(contours[i])

    return  filtered_contours

def load_parameters():
    pass

def nothing(x):
    pass

if __name__ == '__main__':
    start = time.time()
    #src = 'frame.jpg' # This frame contains distorsion and is not parallel to image plane
    #src = 'C:/Users/jaga0/Pictures/Camera Roll/3m/10-11/frames/frame (17).jpg'
    #src = 'C:/Users/jaga0/Pictures/Camera Roll/UPIITA/WIN_20190812_16_56_21_Pro.jpg'  # OK
    #src = './frames/frame0.jpg' # OK
    #src = './frames/frame2.jpg' # NOT OK
    #src = 'C:/Users/jaga0/Pictures/Camera Roll/3m/10-11/video/3m.mp4'
    src = 'D:/100MEDIA/1m_240p.mp4'
    #src = 'C:/Users/jaga0/Pictures/Camera Roll/Triangle_test.mp4'

    objPts = np.array([
                        (0.0, 0.0, 0.0),        # Centroid
                        (0.0, 4.8, 0.0),        # Vertex_1
                        (5.8, -2.3, 0.0),       # Vertex_2
                        (-5.8, -2.3, 0.0)       # Vertex_3
                        ])

    fig = plt.figure()
    ax1 = fig.add_subplot(1,1,1)
    calib_path = "../camera_calibration/"
    camera_matrix = np.loadtxt(calib_path + 'cameraMatrix.txt', delimiter=',')
    dist_coeffs = np.loadtxt(calib_path + 'cameraDistortion.txt', delimiter=',')
    cv.namedWindow('Frame', cv.WINDOW_FULLSCREEN)
    cv.namedWindow('Binary_Frame', cv.WINDOW_FULLSCREEN)
    cv.namedWindow('Contours_Frame', cv.WINDOW_FULLSCREEN)

    [success, error, k] = (0,0,1)

    cap = cv.VideoCapture(src)  # Capture the frame source

    threshold_trackbar = cv.createTrackbar('Threshold','Frame',0,255,nothing)

    if not cap.isOpened():
        print("Error while opening video stream or file")

    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            try:
                #if frame.shape == (1080, 1920, 3):
                #    frame = cv.resize(frame, (426, 240))
                height, width, _ = frame.shape
                #binary_threshold = cv.getrackbarPos('Threshold','Frame')
                binary_threshold = 220  # 200, 230(shadow image)
                gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
                ret, binary_frame = cv.threshold(gray_frame, binary_threshold, 255, cv.THRESH_BINARY)  # Global threshold set to the middle values. {230}
                contours, _ = cv.findContours(binary_frame, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE) # cv.RETR_EXTERNAL
                contours = filter_contours_mean_size(contours)
                binary_frame_contours = contour_analysis(contours, height, width)
                [contour_index, imgPts] = get_signature(contours, ax1)


                (success, rotation_vector, translation_vector) = cv.solvePnP(objPts, imgPts, camera_matrix, dist_coeffs, flags=cv.SOLVEPNP_ITERATIVE)

                print("X: {} Y: {} Z:{}".format(translation_vector[0], translation_vector[1], translation_vector[2]))
                #print("Rotation: {}".format(rotation_vector))

                imgPts = imgPts.astype(int)

                #mask2 = np.zeros((height, width), np.uint8)
                #mask2 = cv.cvtColor(mask2, cv.COLOR_GRAY2BGR)
                #cv.drawContours(mask2, contours, contour_index, (255, 255, 255), 1)


                for i in range(len(imgPts[:,0])):
                    cv.circle(frame, (imgPts[i, 0], imgPts[i, 1]), 5, (255, 0, 0), -1)

                # cv.drawContours(mask, contours, i, (255, 255, 255), 1)
                #binary_frame = cv.cvtColor(binary_frame, cv.COLOR_GRAY2BGR)

                # cv.drawContours(binary_frame, contours, 8, (255,0,0), 2)
                # bwcc= contoursCentroid(contours, bwc.copy())
                # im_show([frame, bw, bwc])

                # cv.imshow("Binary Frame", bw)
                # cv.imshow("bw", bw)
                # cv.waitKey(0)
                # plt.imshow(bw,cmap='gray')
                plt.show()  # To plot all the graphs.
                success += 1
            except:
                error += 1
            k += 1
            cv.imshow('Frame', frame)
            cv.imshow('Binary_Frame', binary_frame)
            cv.imshow('Contours_Frame', binary_frame_contours)
            if cv.waitKey(25)& 0xFF == ord('q'):
                break
        else:
            break

    success_rate = success / k * 100
    error_rate = error / k * 100
    print("Success Rate {}".format(success_rate))
    print("Error Rate {}".format(error_rate))
    cap.release()
    cv.destroyAllWindows()
    finish = time.time() - start
    print("Elapsed time:" + str(finish))

#if __name__ == "__main__":
    #start = time.time()
    #main()
    #finish = time.time() - start
    #print("Elapsed time:" + str(finish))
