#!/usr/bin/env python

import cv2 as cv
import numpy as np
from utils import get_four_points


if __name__ == '__main__' :

    # Read in the image.
    frame = cv.imread("./frames/frame1.jpg ")
    #frame = cv.resize(frame, (640,360))

    # Show image and wait for 4 clicks.
    #cv.namedWindow("Image",cv.WINDOW_NORMAL)
    cv.imshow("Image", frame)
    imgPts = get_four_points(frame)
    objPts = np.array([
                        (0.0, 0.0, 0.0),        # Centroid points
                        (0.0, 4.8, 0.0),        # Vertex 1
                        (5.8, -2.3, 0.0),       #Vertex 2
                        (-5.8, -2.3, 0.0)
                        ])

    calib_path = "../camera_calibration/"
    camera_matrix = np.loadtxt(calib_path + 'cameraMatrix.txt', delimiter=',')
    dist_coeffs = np.loadtxt(calib_path + 'cameraDistortion.txt', delimiter=',')

    (success, rotation_vector, translation_vector) = cv.solvePnP(objPts, imgPts, camera_matrix, dist_coeffs, flags=cv.SOLVEPNP_ITERATIVE)

    # Project 3D points onto the image plane
    (vtX, jacobianX) = cv.projectPoints(np.array([10.0, 0.0, 0.0]), rotation_vector, translation_vector, camera_matrix, dist_coeffs)
    (vtY, jacobianY) = cv.projectPoints(np.array([0.0, 10.0, 0.0]), rotation_vector, translation_vector, camera_matrix, dist_coeffs)
    (vtZ, jacobianZ) = cv.projectPoints(np.array([0.0, 0.0, 10.0]), rotation_vector, translation_vector, camera_matrix, dist_coeffs)

    pC = (int(imgPts[0][0]), int(imgPts[0][1]))
    pX = (int(vtX[0][0][0]), int(vtX[0][0][1]))
    pY = (int(vtY[0][0][0]), int(vtY[0][0][1]))
    pZ = (int(vtZ[0][0][0]), int(vtZ[0][0][1]))
    cv.line(frame, pC,pX, (255,0,0), 5)
    cv.line(frame, pC,pY, (0,255,0), 5)
    cv.line(frame, pC,pZ, (0,0,255), 5)

    cv.waitKey(0)
    cv.destroyAllWindows()
