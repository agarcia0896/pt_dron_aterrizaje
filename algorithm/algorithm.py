# Author : José Antonio García Acosta.
# Date: september 5th, 2019.
# Title: "Algoritmo para la estimación de coordenadas tridimensionales con visión artificial".

import cv2 as cv
import imutils
import math
import time
import numpy as np
import sys

#Set printer properties
#np.set_printoptions(threshold=sys.maxsize)
#np.set_printoptions(precision=2)
#np.set_printoptions(suppress=True)

def imshow(data):
    cv.imshow("RGB Frame", data[0])
    cv.imshow("Binary Frame", data[1])

def contourAnalysis(contours,H,W):
    for i in range(len(contours)):
        mask = np.zeros((H,W),np.uint8)
        cv.drawContours(mask,contours,i,(255,255,255),1)
        title = "Contour " + str(i)
        cv.imshow(title, mask)
        cv.waitKey(0)
        cv.destroyWindow(title)


def main():
    src = ['C:/Users/jaga0/Videos/videoTest.mp4',                               # 0, Circle marker
           'C:/Users/jaga0/Pictures/Camera Roll/3m/10-11/video/3m.mp4',         # 1, 3 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/4m/video/4m.mp4',               # 2, 4 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/6m/10-11/video/6m.mp4',         # 3, 6 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/7m/9-10 am/video/7m_1.mp4',     # 4, 7 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/7m/9-10 am/video/7m_2.mp4',     # 5, 7 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/9m/10-11/video/9m.mp4',         # 6, 9 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/10m/9-10 am/video/10m_1.mp4',   # 7, 10 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/10m/9-10 am/video/10m_2.mp4',   # 8, 10 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/14m/9-10 am/video/14m_1.mp4',   # 9, 14 meters away from marker
           'C:/Users/jaga0/Pictures/Camera Roll/14m/9-10 am/video/14m_2.mp4'    # 10, 14 meters away from marker
           ]
    cap = cv.VideoCapture(src[1]) # Capture the video or camera source

    if cap.isOpened() == False:
        print("Error while opening video stream or file")

    while cap.isOpened():
        # Capture frame-by-frame
        ret, frame = cap.read()
        H,W,Ch = frame.shape
        if ret == True:
            gray = cv.cvtColor(frame,cv.COLOR_BGR2GRAY)
            ret, bw = cv.threshold(gray,127,255,cv.THRESH_BINARY) # Global threshold set to the middle values.
            contours, hierarchy = cv.findContours(bw,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
            #cnt = imutils.grab_contours(contours)
            #contourAnalysis(contours,H,W) # i = 243

            mask = np.zeros((H,W),np.uint8)

            M = cv.moments(contours[243])
            Cx = int(M["m10"] / M["m00"])
            Cy = int(M["m01"] / M["m00"])

            cv.drawContours(mask,contours,243,(255,255,255),1)
            cv.circle(mask,(Cx,Cy),2,(255,255,255), -1)
            title = "Triangle contour"
            cv.imshow(title, mask)
            cv.waitKey(0)

            imshow([frame,bw])
            if cv.waitKey(25)& 0xFF == ord('q'):
                break
        else:
            break
    cap.release()
    cv.destroyAllWindows()

if __name__ == "__main__":
    start = time.time()
    main()
    finish = time.time() - start
    print(finish)
